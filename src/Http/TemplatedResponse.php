<?php declare(strict_types=1);

namespace Impressible\ImpressibleRoute\Http;

/**
 * Represents a Wordpress response with query template.
 * Resolves the template by calling get_query_template().
 * For using with the routing logics in this plugin.
 *
 * @see https://developer.wordpress.org/reference/functions/get_query_template/
 */
class TemplatedResponse
{

    /**
     * Filename without extension.
     *
     * @var string
     */
    private $type;

    /**
     * An optional list of template candidates.
     *
     * @var string[]
     */
    private $templates = [];

    /**
     * Class constructor.
     *
     * @param string   $type      Filename without extension.
     * @param string[] $templates (Optional) An optional list of template candidates.
     *                            Default value: array()
     */
    function __construct(string $type, array $templates = [])
    {
        $this->type = $type;
        $this->templates = $templates;
    }

    /**
     * Returns the HTTP status code to use for the
     * response.
     *
     * @return integer
     */
    function getStatusCode(): int
    {
        return 200;
    }

    /**
     * Returns a full path to template file from get_query_template().
     * Or null if get_query_template() is not a defined function.
     *
     * @return string|null
     */
    function getTemplate(): ?string
    {
        return \function_exists('get_query_template')
            ? \get_query_template($this->type, $this->templates)
            : null;
    }

    /**
     * Get template filename specified.
     *
     * @return string
     */
    function getFilename(): string
    {
        return $this->type . '.php';
    }

    /**
     * Magic method. Uses the getFilename() method internally.
     *
     * @return string
     * @see getFilename()
     */
    public function __toString()
    {
        return $this->getFilename();
    }
}
